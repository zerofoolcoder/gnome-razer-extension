const { GLib, GObject, St } = imports.gi;
const { panelMenu: PanelMenu, popupMenu: PopupMenu, main: Main } = imports.ui;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Logger = Extension.imports.logger.Logger;
const MenuItem = Extension.imports.menuitem.MenuItem;
const MenuSlider = Extension.imports.menuslider.MenuSlider;
const NightLight = Extension.imports.nightlight.NightLight;
const { RazerDaemon, RazerDevices } = Extension.imports.orgrazer;
const RazerDevice = Extension.imports.razerdevice.RazerDevice;
const SettingsManager = Extension.imports.settingsmanager.SettingsManager;
const Utilities = Extension.imports.utilities;

const RAZER_ICON_OFF = 'razer-icon-off';
const RAZER_ICON_ON = 'razer-icon-on';

let GnomeRazer = GObject.registerClass(
class GnomeRazer extends PanelMenu.Button {
    _init(_settings, _logger) {
        super._init(0.0, _settings.name);

        this._settings = _settings;
        this._logger = _logger;

        this._nightLight = new NightLight();
        this._nightLight.connect(this._onNightLightChanged.bind(this));

        this._loadDevice();
        this._createPanelMenuBox();
        this._createMenuSection();
        this._update();
    }

    _loadDevice() {
        this._razerDaemon = new RazerDaemon();
        this._razerDevices = new RazerDevices();
        this._razerDeviceSerial = this._razerDevices.getDevices().toString();
        this._razerDevice = new RazerDevice(this._razerDeviceSerial);
    }

    _createPanelMenuBox() {
        let box = new St.BoxLayout({
            style_class: 'panel-status-menu-box'
        });
        
        this.icon = new St.Icon({
            style_class: 'razer-icon-off'
        });
        
        box.add_child(this.icon);
        box.add_child(PopupMenu.arrowIcon(St.Side.BOTTOM));
        this.add_child(box);
    }

    _createMenuSection() {
        this.menuItems = [];
        this.menuSection = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this.menuSection);

        this._createDeviceInfoSubmenu();
        this._createSettingsSubmenu();
        this._createEffectsSubmenu();
    }

    _createEffectsSubmenu() {
        const submenu = new PopupMenu.PopupSubMenuMenuItem('Effects');

        for (let effect of this._razerDevice) {
            submenu.menu.addMenuItem(this._createMenuItem(effect));
        }

        this.menuSection.addMenuItem(submenu);
    }

    _createSettingsSubmenu() {
        const submenu = new PopupMenu.PopupSubMenuMenuItem('Settings');

        const menuSlider = new MenuSlider('brightness', this._settings.brightness / this._settings.maxBrightness);
        menuSlider._slider.connect('value-changed', this._onBrightnessChanged.bind(this));
        submenu.menu.addMenuItem(menuSlider);

        this._nightLightMenu = new PopupMenu.PopupSwitchMenuItem('Night Light', this._settings.nightlight, {
            reactive: true,
            activate: true,
            hover: true,
            can_focus: true
        });
        this._nightLightMenu.connect('activate', this._onNightLightSwitchChanged.bind(this));
        submenu.menu.addMenuItem(this._nightLightMenu);

        this.menuSection.addMenuItem(submenu);
    }

    _createDeviceInfoSubmenu() {
        const submenu = new PopupMenu.PopupSubMenuMenuItem(this._razerDevice.getDeviceName().toString());

        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Game mode: ${this._razerDevice.getGameMode()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Driver: ${this._razerDevice.getDriverVersion()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Serial: ${this._razerDevice.getSerial()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Firmware: ${this._razerDevice.getFirmware()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Keyboard layout: ${this._razerDevice.getKeyboardLayout()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Device type: ${this._razerDevice.getDeviceType()}`));
        submenu.menu.addMenuItem(new PopupMenu.PopupMenuItem(`Daemon: ${this._razerDaemon.version()}`));

        this.menuSection.addMenuItem(submenu);
    }

    _createMenuItem(effect) {
        const menuItem = new MenuItem('effect', effect.value, effect.text);
        menuItem.connect('activate', this._onEffectChanged.bind(this, menuItem));

        this.menuItems.push(menuItem);
        return menuItem;
    }

    _onNightLightSwitchChanged() {
        this._settings.nightlight = this._nightLightMenu.state;
        this._update();
        this._logger.log(`follow Night Light set to ${this._settings.nightlight}`);
    }

    _onNightLightChanged(interface_name, changed_properties, invalidated_properties) {
        if (!this._settings.nightlight) {
            return;
        }

        if (!changed_properties.unpack().hasOwnProperty('NightLightActive')) {
            return;
        }

        this._update();
        this._logger.log(`follow night light ${this._settings.nightlight}`);
    }

    _onBrightnessChanged(slider, value, property) {
        const newValue = Math.round(value * this._settings.maxBrightness);
        if(newValue == this._settings.brightness) {
            return;
        }

        this._settings.brightness = newValue;
        this._updateBrightness();
    }

    _onEffectChanged(menuItem) {
        if(this._settings.effect == menuItem._value) {
            return;
        }

        this._settings.effect = menuItem._value;
        this._update();
    }

    _updateMenuItems() {
        this.menuItems.forEach(menuItem => {
            const ornament = menuItem._value == this._settings.effect ? PopupMenu.Ornament.DOT : PopupMenu.Ornament.NONE;
            menuItem.setOrnament(ornament);
        });
    }

    _updateRazerIcon() {
        if (this._settings.effect == this._settings.effectDefault || (this._settings.nightlight && !this._nightLight.NightLightActive)) {
            this.icon.style_class = RAZER_ICON_OFF;
            return;
        }

        this.icon.style_class = RAZER_ICON_ON;
    }

    _updateEffect() {
        if (this._settings.nightlight && !this._nightLight.NightLightActive) {
            this._razerDevice[this._settings.effectDefault]();
            this._logger.log(`effect set to ${this._settings.effectDefault}`);
            return;
        }

        this._razerDevice[this._settings.effect]();
        this._logger.log(`effect set to ${this._settings.effect}`);
    }

    _updateBrightness() {
        this._razerDevice.setBrightness(this._settings.brightness);
        this._logger.log(`brightness set to ${this._settings.brightness}`, false);
    }

    _update() {
        this._updateBrightness();
        this._updateEffect();
        this._updateMenuItems();
        this._updateRazerIcon();
    }
});

let _settings;
let _logger;

function init() {
    _settings = new SettingsManager(Utilities.SCHEMA);
    _logger = new Logger(_settings.name, _settings.notify, _settings.log);
    _logger.log('starting', false, true);
}

let _indicator;

function enable() {
    _indicator = new GnomeRazer(_settings, _logger);
    Main.panel.addToStatusArea('gnome-razer', _indicator);
    _logger.log('device ready', false, true);
}

function disable() {
    _indicator.destroy();
    _logger.log('disabled', false, true);
}