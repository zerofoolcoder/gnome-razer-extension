const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();

var SettingsManager = class {
    constructor(schema) {
        this._schema = schema;
        this._settings = ExtensionUtils.getSettings(this._schema);
        this._metadata = Extension.metadata;
    }

    get brightness() {
        return this._settings.get_int('brightness');
    }

    set brightness(value) {
        if(typeof value != "number" || value < 0 || value > 255) {
            return;
        }
        
        this._settings.set_int("brightness", value);
    }
    
    get maxBrightness() {
        return this._settings.get_int('max-brightness');
    }
    get effectDefault() {
        return this._settings.get_default_value('effect').unpack();
    }

    get effect() {
        return this._settings.get_string('effect');
    }

    set effect(value) {
        if(typeof value != "string") {
            return;
        }

        this._settings.set_string("effect", value);
    }

    get log() {
        return this._settings.get_boolean('log');
    }

    set log(value) {
        if(typeof value != "boolean") {
            return;
        }

        this._settings.set_boolean("log", value);
    }

    get notify() {
        return this._settings.get_boolean('notify');
    }

    set notify(value) {
        if(typeof value != "boolean") {
            return;
        }

        this._settings.set_boolean("notify", value);
    }

    get nightlight() {
        return this._settings.get_boolean('nightlight');
    }

    set nightlight(value) {
        if(typeof value != "boolean") {
            return;
        }

        this._settings.set_boolean("nightlight", value);
    }

    get description() {
        return this._metadata.description;
    }

    get extension_id() {
        return this._metadata["extension-id"];
    }

    get name() {
        return this._metadata.name;
    }

    get original_author() {
        return this._metadata["original-author"];
    }

    get shell_version() {
        return this._metadata["shell-version"];
    }

    get url() {
        return this._metadata.url;
    }

    get uuid() {
        return this._metadata.uuid;
    }

    get version() {
        return this._metadata.version;
    }

    get path() {
        return this._metadata.path;
    }
}