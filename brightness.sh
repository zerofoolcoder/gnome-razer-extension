#!/bin/bash

#this removes the double quotes
Brightness="${1//\"}"

echo "$(date +%b\ %d\ %T) Razer Blackwidow Chroma V2 setting brightness: $Brightness" >> ~/.local/share/gnome-shell/extensions/gnomerazer@contacto.rodolfocerezo.com/razer.log
echo -n "$Brightness" > /sys/devices/pci0000:00/0000:00:14.0/usb1/1-1/1-1:1.2/0003:1532:0221.0003/matrix_brightness