const Main = imports.ui.main;

var Logger = class {
    constructor(name, notify, log) {
        this._name = name;
        this._notify = notify;
        this._log = log;
    }

    log(message, _notify = this._notify, _log = this._log) {
        const _message = this._name + ": " + message;

        if (_notify) {
            Main.notify(_message);
        }

        if (_log) {
            log(_message);
        }
    }
}