const St = imports.gi.St;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const BaseMenuItem = Extension.imports.basemenuitem.BaseMenuItem;

var MenuItem = class extends BaseMenuItem {
    constructor(key, value, text) {
        super({
            activate: true,
            hover: true,
            can_focus: true
        }, ...arguments);

        this._createActors();
    }

    _createActors() {
        this.actor.add_child(new St.Label({
            text: this._text,
            x_expand: true
        }));
    }
}