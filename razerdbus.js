const Gio = imports.gi.Gio;

var RazerDBus = class {
    constructor(busName, path, iface, device = '') {
        this._device = device;
        this._iface = iface;
        this._busName = busName;
        this._path = path;
        const razerProxy = Gio.DBusProxy.makeProxyWrapper(this._iface);
        this._proxy = new razerProxy(Gio.DBus.session, this._busName, `${this._path}${device}`);
    }
}
