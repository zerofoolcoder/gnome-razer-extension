const Slider = imports.ui.slider;
const St = imports.gi.St;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const BaseMenuItem = Extension.imports.basemenuitem.BaseMenuItem;

var MenuSlider = class extends BaseMenuItem {
    constructor(key, value, text) {
        super({ activate: false }, ...arguments);

        this._createActors();
    }

    _createActors() {
        this._slider = new Slider.Slider(this._value);
        this.actor.add(new St.Icon({
            icon_name: 'display-brightness-symbolic',
            style_class: 'popup-menu-icon'
        }));
        this.actor.add(this._slider.actor, { expand: true });
    }
}