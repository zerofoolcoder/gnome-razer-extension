const Extension = imports.misc.extensionUtils.getCurrentExtension();
const RazerDBus = Extension.imports.razerdbus.RazerDBus;
 
const IORG_FREEDESKTOP_DBUS_INTROSPECTABLE = `
<node>
    <interface name="org.freedesktop.DBus.Introspectable">
        <method name="Introspect">
            <arg direction="out" type="s" />
        </method>
    </interface>
</node>`;

const IRAZER_DAEMON = `
<node>
    <interface name="razer.daemon">
        <method name="version">
            <arg direction="out" type="s" />
        </method>
        <method name="stop">
        </method>
    </interface>
</node>`;

const IRAZER_DEVICES = `
<node>
    <interface name="razer.devices">
        <signal name="device_removed">
        </signal>
        <signal name="device_added">
        </signal>
        <method name="syncEffects">
            <arg direction="in"  type="b" name="enabled" />
        </method>
        <method name="getOffOnScreensaver">
            <arg direction="out" type="b" />
        </method>
        <method name="getDevices">
            <arg direction="out" type="as" />
        </method>
        <method name="enableTurnOffOnScreensaver">
            <arg direction="in"  type="b" name="enable" />
        </method>
        <method name="supportedDevices">
            <arg direction="out" type="s" />
        </method>
        <method name="getSyncEffects">
            <arg direction="out" type="b" />
        </method>
    </interface>
</node>`;

var OrgFreedesktopDbusIntrospectable = class extends RazerDBus {
    constructor() {
        super('org.razer', '/org/razer', IORG_FREEDESKTOP_DBUS_INTROSPECTABLE);
    }

    Introspect() {
        return this._proxy.IntrospectSync();
    }
}

var RazerDaemon = class extends RazerDBus {
    constructor() {
        super('org.razer', '/org/razer', IRAZER_DAEMON);
    }

    version() {
        return this._proxy.versionSync();
    }

    stop() {
        return this._proxy.stopSync();
    }
}

var RazerDevices = class extends RazerDBus {
    constructor() {
        super('org.razer', '/org/razer', IRAZER_DEVICES);
    }

    enableTurnOffOnScreensaver(enable = true) {
        return this._proxy.enableTurnOffOnScreensaverSync(enable);
    }

    getDevices() {
        return this._proxy.getDevicesSync();
    }    

    getOffOnScreensaver() {
        return this._proxy.getOffOnScreensaverSync();
    }        

    getSyncEffects() {
        return this._proxy.getSyncEffectsSync();
    }

    supportedDevices() {
        return this._proxy.supportedDevicesSync();
    }    

    syncEffects(enabled = true) {
        return this._proxy.syncEffectsSync(enabled);
    }                    
}