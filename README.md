# Gnome Razer

This gnome extension adds a razer indicator in the top bar and lets you interact 
with your Razer Blackwidow Chroma V2 keyboard (for now).

## Getting Started

Since GNOME Shell is not API stable, extensions work only against a very
specific version of the shell. The extensions in this package are supported by GNOME
and will be updated to reflect future API changes in GNOME Shell.

### Prerequisites

What things you need to install before this extension:

```
[Gnome shell v3.32.1](https://www.gnome.org/)
[openrazer driver v2.5.0](https://openrazer.github.io/)

Check if the openrazer-daemon driver is enabled:
systemctl --user is-enabled openrazer-daemon.service

To enable the daemon:
sudo systemctl --user enable openrazer-daemon.service

Reboot
```

### Installing

For a local installation:

```
cd ~/.local/share/gnome-shell/extensions
git clone https://zerofoolcoder@bitbucket.org/zerofoolcoder/gnome-razer-extension.git
```

For a global installation:

```
cd /usr/share/gnome-shell/extensions
sudo git clone https://zerofoolcoder@bitbucket.org/zerofoolcoder/gnome-razer-extension.git

```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Disclaimer

I'm using this extension without any problem, but it is still experimental so use it at your own risk.