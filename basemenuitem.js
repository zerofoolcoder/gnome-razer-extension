const PopupMenu = imports.ui.popupMenu;

var BaseMenuItem = class extends PopupMenu.PopupBaseMenuItem {
    constructor(params, key, value, text=null) {
        super(params);

        this._key = key;
        this._value = value;
        this._text = text;
    }
}