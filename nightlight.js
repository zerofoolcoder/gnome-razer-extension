const Extension = imports.misc.extensionUtils.getCurrentExtension();
const RazerDBus = Extension.imports.razerdbus.RazerDBus;
 
const { loadInterfaceXML } = imports.misc.fileUtils;

const BUS_NAME = "org.gnome.SettingsDaemon.Color";
const OBJECT_PATH = "/org/gnome/SettingsDaemon/Color";
const INTERFACE = loadInterfaceXML(BUS_NAME);

var NightLight = class extends RazerDBus {
    constructor() {
        super(BUS_NAME, OBJECT_PATH, INTERFACE);
    }
    
    get NightLightActive() {
        return this._proxy.NightLightActive;
    }

    connect(nightLightCallback) {
        this._proxy.connect('g-properties-changed', nightLightCallback);
    }
}