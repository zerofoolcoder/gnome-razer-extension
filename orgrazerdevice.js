const Extension = imports.misc.extensionUtils.getCurrentExtension();
const RazerDBus = Extension.imports.razerdbus.RazerDBus;
 
const IORG_FREEDESKTOP_DBUS_INTROSPECTABLE = `
<node>
    <interface name="org.freedesktop.DBus.Introspectable">
        <method name="Introspect">
            <arg direction="out" type="s" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_LED_GAMEMODE = `
<node>
    <interface name="razer.device.led.gamemode">
        <method name="getGameMode">
            <arg direction="out" type="b" />
        </method>
        <method name="setGameMode">
            <arg direction="in"  type="b" name="enable" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_LED_MACROMODE = `
<node>
    <interface name="razer.device.led.macromode">
        <method name="getMacroMode">
            <arg direction="out" type="b" />
        </method>
        <method name="setMacroMode">
            <arg direction="in"  type="b" name="enable" />
        </method>
        <method name="getMacroEffect">
            <arg direction="out" type="i" />
        </method>
        <method name="setMacroEffect">
            <arg direction="in"  type="y" name="effect" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_LIGHTING_BRIGHTNESS = `
<node>
    <interface name="razer.device.lighting.brightness">
        <method name="getBrightness">
            <arg direction="out" type="d" />
        </method>
        <method name="setBrightness">
            <arg direction="in"  type="d" name="brightness" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_LIGHTING_CHROMA = `
<node>
    <interface name="razer.device.lighting.chroma">
        <method name="setWave">
            <arg direction="in"  type="i" name="direction" />
        </method>
        <method name="setStatic">
            <arg direction="in"  type="y" name="red" />
            <arg direction="in"  type="y" name="green" />
            <arg direction="in"  type="y" name="blue" />
        </method>
        <method name="setSpectrum">
        </method>
        <method name="setReactive">
            <arg direction="in"  type="y" name="red" />
            <arg direction="in"  type="y" name="green" />
            <arg direction="in"  type="y" name="blue" />
            <arg direction="in"  type="y" name="speed" />
        </method>
        <method name="setNone">
        </method>
        <method name="setBreathRandom">
        </method>
        <method name="setBreathSingle">
            <arg direction="in"  type="y" name="red" />
            <arg direction="in"  type="y" name="green" />
            <arg direction="in"  type="y" name="blue" />
        </method>
        <method name="setBreathDual">
            <arg direction="in"  type="y" name="red1" />
            <arg direction="in"  type="y" name="green1" />
            <arg direction="in"  type="y" name="blue1" />
            <arg direction="in"  type="y" name="red2" />
            <arg direction="in"  type="y" name="green2" />
            <arg direction="in"  type="y" name="blue2" />
        </method>
        <method name="setCustom">
        </method>
        <method name="setKeyRow">
            <arg direction="in"  type="ay" name="payload" />
        </method>
        <method name="setStarlightRandom">
            <arg direction="in"  type="y" name="speed" />
        </method>
        <method name="setStarlightSingle">
            <arg direction="in"  type="y" name="speed" />
            <arg direction="in"  type="y" name="red" />
            <arg direction="in"  type="y" name="green" />
            <arg direction="in"  type="y" name="blue" />
        </method>
        <method name="setStarlightDual">
            <arg direction="in"  type="y" name="speed" />
            <arg direction="in"  type="y" name="red1" />
            <arg direction="in"  type="y" name="green1" />
            <arg direction="in"  type="y" name="blue1" />
            <arg direction="in"  type="y" name="red2" />
            <arg direction="in"  type="y" name="green2" />
            <arg direction="in"  type="y" name="blue2" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_LIGHTING_CUSTOM = `
<node>
    <interface name="razer.device.lighting.custom">
        <method name="setRipple">
            <arg direction="in"  type="y" name="red" />
            <arg direction="in"  type="y" name="green" />
            <arg direction="in"  type="y" name="blue" />
            <arg direction="in"  type="d" name="refresh_rate" />
        </method>
        <method name="setRippleRandomColour">
            <arg direction="in"  type="d" name="refresh_rate" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_MACRO = `
<node>
    <interface name="razer.device.macro">
        <method name="getMacros">
            <arg direction="out" type="s" />
        </method>
        <method name="deleteMacro">
            <arg direction="in"  type="s" name="macro_key" />
        </method>
        <method name="addMacro">
            <arg direction="in"  type="s" name="macro_bind_key" />
            <arg direction="in"  type="s" name="macro_json" />
        </method>
    </interface>
</node>`;

const IRAZER_DEVICE_MISC = `
<node>
    <interface name="razer.device.misc">
        <method name="getVidPid">
            <arg direction="out" type="ai" />
        </method>
        <method name="resumeDevice">
        </method>
        <method name="setDeviceMode">
            <arg direction="in"  type="y" name="mode_id" />
            <arg direction="in"  type="y" name="param" />
        </method>
        <method name="hasDedicatedMacroKeys">
            <arg direction="out" type="b" />
        </method>
        <method name="getDeviceMode">
            <arg direction="out" type="s" />
        </method>
        <method name="suspendDevice">
        </method>
        <method name="getDriverVersion">
            <arg direction="out" type="s" />
        </method>
        <method name="getSerial">
            <arg direction="out" type="s" />
        </method>
        <method name="getRazerUrls">
            <arg direction="out" type="s" />
        </method>
        <method name="getFirmware">
            <arg direction="out" type="s" />
        </method>
        <method name="getMatrixDimensions">
            <arg direction="out" type="ai" />
        </method>
        <method name="hasMatrix">
            <arg direction="out" type="b" />
        </method>
        <method name="getDeviceName">
            <arg direction="out" type="s" />
        </method>
        <method name="getKeyboardLayout">
            <arg direction="out" type="s" />
        </method>
        <method name="getDeviceType">
            <arg direction="out" type="s" />
        </method>
    </interface>
</node>`;

var Introspectable = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IORG_FREEDESKTOP_DBUS_INTROSPECTABLE, device);
    }

    Introspect() {
        return this._proxy.IntrospectSync();
    }
}

var LedGameMode = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_LED_GAMEMODE, device);
    }

    getGameMode() {
        return this._proxy.getGameModeSync();
    }

    setGameMode(enable = false) {
        return this._proxy.setGameModeSync(enable);
    }
}

var LedMacroMode = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_LED_MACROMODE, device);
    }

    getMacroMode() {
        return this._proxy.getMacroModeSync();
    }

    setMacroMode(enable = false) {
        return this._proxy.setMacroModeSync(enable);
    }

    getMacroEffect() {
        return this._proxy.getMacroEffectSync();
    }

    setMacroEffect(effect = 0) {
        return this._proxy.setMacroEffectSync(effect);
    }
}

var LightingBrightness = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_LIGHTING_BRIGHTNESS, device);
    }

    getBrightness() {
        return this._proxy.getBrightnessSync();
    }

    setBrightness(brightness) {
        return this._proxy.setBrightnessSync(brightness);
    }
}

var LightingChroma = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_LIGHTING_CHROMA, device);
    }

    setWave(direction) {
        return this._proxy.setWaveSync(direction);
    }

    setStatic(red, green, blue) {
        return this._proxy.setStaticSync(red, green, blue);
    }

    setSpectrum() {
        return this._proxy.setSpectrumSync();
    }

    setReactive(red, green, blue, speed) {
        return this._proxy.setReactiveSync(red, green, blue, speed);
    }

    setNone() {
        return this._proxy.setNoneSync();
    }

    setBreathRandom() {
        return this._proxy.setBreathRandomSync();
    }

    setBreathSingle(red, green, blue) {
        return this._proxy.setBreathSingleSync(red, green, blue);
    }

    setBreathDual(red1, green1, blue1, red2, green2, blue2) {
        return this._proxy.setBreathDualSync(red1, green1, blue1, red2, green2, blue2);
    }

    setCustom() {
        return this._proxy.setCustomSync();
    }

    setKeyRow(payload = []) {
        return this._proxy.setKeyRowSync(payload);
    }

    setStarlightRandom(speed) {
        return this._proxy.setStarlightRandomSync(speed);
    }

    setStarlightSingle(speed, red, green, blue) {
        return this._proxy.setStarlightSingleSync(speed, red, green, blue);
    }

    setStarlightDual(speed, red1, green1, blue1, red2, green2, blue2) {
        return this._proxy.setStarlightDualSync(speed, red1, green1, blue1, red2, green2, blue2);
    }
}

var LightingCustom = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_LIGHTING_CUSTOM, device);
    }

    setRipple(red, green, blue, refresh_rate) {
        return this._proxy.setRippleSync(red, green, blue, refresh_rate);
    }

    setRippleRandomColour(refresh_rate) {
        return this._proxy.setRippleRandomColourSync(refresh_rate);
    }
}

var Macro = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_MACRO, device);
    }

    getMacros() {
        return this._proxy.getMacrosSync();
    }

    deleteMacro(macro_key) {
        return this._proxy.deleteMacroSync(macro_key);
    }

    addMacro(macro_bind_key, macro_json) {
        return this._proxy.addMacroSync(macro_bind_key, macro_json);
    }
}

var Misc = class extends RazerDBus {
    constructor(device) {
        super('org.razer', '/org/razer/device/', IRAZER_DEVICE_MISC, device);
    }

    getVidPid() {
        return this._proxy.getVidPidSync();
    }

    resumeDevice() {
        return this._proxy.resumeDeviceSync();
    }

    setDeviceMode(mode_id, param) {
        return this._proxy.setDeviceModeSync(mode_id, param);
    }

    hasDedicatedMacroKeys() {
        return this._proxy.hasDedicatedMacroKeysSync();
    }

    getDeviceMode() {
        return this._proxy.getDeviceModeSync();
    }

    suspendDevice() {
        return this._proxy.suspendDeviceSync();
    }

    getDriverVersion() {
        return this._proxy.getDriverVersionSync();
    }

    getSerial() {
        return this._proxy.getSerialSync();
    }

    getRazerUrls() {
        return this._proxy.getRazerUrlsSync();
    }

    getFirmware() {
        return this._proxy.getFirmwareSync();
    }

    getMatrixDimensions() {
        return this._proxy.getMatrixDimensionsSync();
    }

    hasMatrix() {
        return this._proxy.hasMatrixSync();
    }

    getDeviceName() {
        return this._proxy.getDeviceNameSync();
    }

    getKeyboardLayout() {
        return this._proxy.getKeyboardLayoutSync();
    }

    getDeviceType() {
        return this._proxy.getDeviceTypeSync();
    }
}