const Extension = imports.misc.extensionUtils.getCurrentExtension();
const { Introspectable, LedGameMode, LedMacroMode, LightingBrightness, LightingChroma, LightingCustom, Macro, Misc } = Extension.imports.orgrazerdevice;
const EFFECTS = [
    { text: 'Breath Dual', value: 'setBreathDual'},
    { text: 'Breath Random', value: 'setBreathRandom'},
    { text: 'Breath Single', value: 'setBreathSingle'},
    { text: 'None', value: 'setNone'},
    { text: 'Reactive', value: 'setReactive'},
    { text: 'Ripple', value: 'setRipple'},
    { text: 'Ripple Random Colour', value: 'setRippleRandomColour'},
    { text: 'Spectrum', value: 'setSpectrum'},
    { text: 'Starlight Dual', value: 'setStarlightDual'},
    { text: 'Starlight Random', value: 'setStarlightRandom'},
    { text: 'Starlight Single', value: 'setStarlightSingle'},
    { text: 'Static', value: 'setStatic'},
    { text: 'Wave', value: 'setWave'}
];

var RazerDevice = class {
    constructor(device) {
        this._device = device;
        this._introspectable = new Introspectable(this._device);
        this._ledGameMod = new LedGameMode(this._device);
        this._ledMacroMode = new LedMacroMode(this._device);
        this._lightningBrightness = new LightingBrightness(this._device);
        this._lightningChroma = new LightingChroma(this._device);
        this._lightningCustom = new LightingCustom(this._device);
        this._macro = new Macro(this._device);
        this._misc = new Misc(this._device);
    }

    *[Symbol.iterator]() {
        for(let i = 0; i < EFFECTS.length; i++) {
            yield EFFECTS[i];
        }
    }

    Introspect() {
        return this._introspectable.Introspect();
    }

    getGameMode() {
        return this._ledGameMod.getGameMode();
    }

    setGameMode(enable = false) {
        return this._ledGameMod.setGameMode(enable);
    }

    getMacroMode() {
        return this._ledMacroMode.getMacroMode();
    }

    setMacroMode(enable = false) {
        return this._ledMacroMode.setMacroMode(enable);
    }

    getMacroEffect() {
        return this._ledMacroMode.getMacroEffect();
    }

    setMacroEffect(effect = 0) {
        return this._ledMacroMode.setMacroEffect(effect);
    }

    getBrightness() {
        return this._lightningBrightness.getBrightness();
    }

    setBrightness(brightness) {
        return this._lightningBrightness.setBrightness(brightness);
    }

    setWave(direction = 1) {
        return this._lightningChroma.setWave(direction);
    }

    setStatic(red = 0, green = 255, blue = 0) {
        return this._lightningChroma.setStatic(red, green, blue);
    }

    setSpectrum() {
        return this._lightningChroma.setSpectrum();
    }

    setReactive(red = 0, green = 255, blue = 0, speed = 1) {
        return this._lightningChroma.setReactive(red, green, blue, speed);
    }

    setNone() {
        return this._lightningChroma.setNone();
    }

    setBreathRandom() {
        return this._lightningChroma.setBreathRandom();
    }

    setBreathSingle(red = 0, green = 255, blue = 0) {
        return this._lightningChroma.setBreathSingle(red, green, blue);
    }

    setBreathDual(red1 = 0, green1 = 255, blue1 = 0, red2 = 128, green2 = 0, blue2 = 128) {
        return this._lightningChroma.setBreathDual(red1, green1, blue1, red2, green2, blue2);
    }

    setStarlightRandom(speed = 1) {
        return this._lightningChroma.setStarlightRandom(speed);
    }

    setStarlightSingle(speed = 1, red = 0, green = 255, blue = 0) {
        return this._lightningChroma.setStarlightSingle(speed, red, green, blue);
    }

    setStarlightDual(speed = 1, red1 = 0, green1 = 255, blue1 = 0, red2 = 128, green2 = 0, blue2 = 128) {
        return this._lightningChroma.setStarlightDual(speed, red1, green1, blue1, red2, green2, blue2);
    }

    setRipple(red = 0, green = 255, blue = 0, refresh_rate = 0.05) {
        return this._lightningCustom.setRipple(red, green, blue, refresh_rate);
    }

    setRippleRandomColour(refresh_rate = 0.05) {
        return this._lightningCustom.setRippleRandomColour(refresh_rate);
    }

    getMacros() {
        return this._macro.getMacros();
    }

    getDeviceMode() {
        return this._misc.getDeviceMode();
    }

    getDriverVersion() {
        return this._misc.getDriverVersion();
    }

    getSerial() {
        return this._misc.getSerial();
    }

    getFirmware() {
        return this._misc.getFirmware();
    }

    getMatrixDimensions() {
        return this._misc.getMatrixDimension();
    }

    hasMatrix() {
        return this._misc.hasMatrix();
    }

    getDeviceName() {
        return this._misc.getDeviceName();
    }

    getKeyboardLayout() {
        return this._misc.getKeyboardLayout();
    }

    getDeviceType() {
        return this._misc.getDeviceType();
    }
}
